<?php

/**
 * Scans a directory for files ending in *.dict and loads them as dictionaries
 * @param string $directory		The directory to scan for *.dict files
 * @return array				An combined array of all words found in scanned dictionary files
 */
function getDictionary(string $directory): array
{
    $directory = realpath($directory);
    $files = scandir($directory);
    $dictionary = '';
    
    // Filter to only read .dict files
    $dictFiles = array_filter($files, function ($file) {
        return (preg_match('/\.dict$/', $file) === 1);
    });

    // Read dictionary files
    foreach ($dictFiles as $file) {
        $dictionary .= file_get_contents($directory . DIRECTORY_SEPARATOR . $file);
    }

    // Convert dictionary to an array with an entry for every newline
    $dictionary = explode("\r\n", $dictionary);
    
    return $dictionary;
}

/**
 * Returns a string representing a RegEx that can be used to search the dictionary for matching words
 * @param string[] $alphabet The characters that may occur in a word
 * @param string[] @word The letters in the word being searched for, with underscore being a wildcard
 * @return string A regular expression used for searching the dictionary
 */
function constructRegex(array $alphabet, array $word): string
{
    $regex = "";
    $length = count($word);
    $alphaString = implode('', $alphabet);
    
    for ($i = 0; $i < $length; $i++) {
        if ($word[$i] === '_') {
            // Any alphabet letter can occur on this position
            $regex .= '[' . $alphaString . ']';
        } else {
            // Only specified letter can occur on this position
            $regex .= $word[$i];
        }
    }

    $regex = '/^' . $regex . '$/i';
    
    return $regex;
}
