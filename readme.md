#Description
This is a very quick-and-dirty solver for hangman puzzles. It searches one or more dictionary files for solutions based on the inputs given for "word" and "guesses".

![Screenshot of the solver](https://i.imgur.com/Fxs8OOm.png)

#How to use
The project does not currently include a user interface for input. The solver expects the following input through GET requests:
word: The word (or part of it) that is being guessed. Comma separated values. Underscores for unknown characters.
guesses: The guesses that have been done so far (both correct and incorrect ones). Comma separated values.

An example of a working request is:
`solver.php?word=_,o,_,_,_&guesses=o,t`

The above request indicates a five letter word, with the second letter being an `o`. The letters `o` and `t` have been guessed, implying that `t` does not occur and `o` only occurs on the second position.

#Dictionaries
Dictionary files are not included with this project. The projects has been designed to work with the word lists provided by https://www.opentaal.org/ (Dutch), but will work with any text file containing words separated by newlines (`\r\n`).

To add a dictionary to the project, save it with a `.dict` extension into the `dictionaries` directory and it will be picked up by the solver.

#License
This project is licensed under the MIT license. See the `license.txt` file for details.
