<?php
require_once("functions.php");

// This directory will be scanned for *.dict files.
// Dictionary files are expected to contain words separated by newlines (\r\n)
$dictionaryDir = 'dictionaries';

// Only show matched dictionary words when the total number of matches is at most this much
$maxMatches = 100;

// All characters that are valid for guessing
$alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

$guesses = isset($_GET['guesses']) ? explode(',', $_GET['guesses']) : [];
$word	 = isset($_GET['word']) ? explode(',', $_GET['word']) : [];
$length	 = count($word);

$dictionary = getDictionary($dictionaryDir);
echo '<p>Dictionary contains '. count($dictionary) .' entries.</p>';

// Construct expression for guesses
// 1. Remove incorrectly guessed letters from the alphabet
$alphabet = array_diff($alphabet, $guesses);
$regex = constructRegex($alphabet, $word);

// Regex has been built, match it against the dictionary
$matches = preg_grep($regex, $dictionary);
$numMatches = count($matches);

// Display results
if ($numMatches <= $maxMatches) {
    echo "<p>Displaying ${numMatches} matches: <ul style='font-family:consolas,\"Lucida Console\", Monaco, monospace;'>";
    
    foreach ($matches as $match) {
        print("<li>${match}</li>");
    }

    echo '</ul></p>';
} else {
    echo "<p>Found ${numMatches} matches.</p>";
}

// Get the optimal next guess
echo '<hr />';

$allMatches			 = $matches;
$optimalGuessMatches = null;
$optimalGuess		 = '';

foreach ($alphabet as $key => $value) {
    $alphabetGuess = $alphabet;
    unset($alphabetGuess[$key]);
    
    $regex = constructRegex($alphabetGuess, $word);
    $guessMatches = count(preg_grep($regex, $allMatches));
    
    if ($guessMatches === 0) {
        // Don't use matches that result in no solutions
        continue;
    }
    
    if (null === $optimalGuessMatches || $guessMatches < $optimalGuessMatches) {
        $optimalGuessMatches = $guessMatches;
        $optimalGuess		 = $alphabet[$key];
    }
}

echo "Optimal next guess: ${optimalGuess} (leaves ${optimalGuessMatches} options)";
